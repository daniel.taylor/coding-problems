// Solution for the parking lot problems.

package parking

import (
	"errors"
)

type ParkingSpot struct {
	column int
	row    int
}

const (
	openGenericSpace   rune = '0'
	closedGenericSpace rune = '1'

	openRegularSpace    rune = 'r'
	openSmallSpace      rune = 's'
	openMotorcycleSpace rune = 'm'

	noSpot = "No available parking spot"
)

func FindEarliestSpotSimple(parkingLot [][]rune) (*ParkingSpot, error) {
	for row, colVals := range parkingLot {
		for col, val := range colVals {
			if val == openGenericSpace {
				return &ParkingSpot{column: col, row: row}, nil
			}
		}
	}
	return nil, errors.New(noSpot)
}

func FindEarliestSpotByStrictType(parkingLot [][]rune, vehicleTypes []rune) []*ParkingSpot {
	emptySpotMap := populateEmptySpots(parkingLot)
	closestSpots := make([]*ParkingSpot, 0)

	for _, vType := range vehicleTypes {
		if spots, ok := emptySpotMap[vType]; ok && len(spots) > 0 {
			val := spots[0]
			emptySpotMap[vType] = emptySpotMap[vType][1:]
			closestSpots = append(closestSpots, val)
		} else {
			closestSpots = append(closestSpots, &ParkingSpot{column: -1, row: -1})
		}
	}
	return closestSpots
}

func populateEmptySpots(parkingLot [][]rune) map[rune][]*ParkingSpot {
	esm := make(map[rune][]*ParkingSpot)
	esm[openSmallSpace] = make([]*ParkingSpot, 0)
	esm[openRegularSpace] = make([]*ParkingSpot, 0)
	esm[openMotorcycleSpace] = make([]*ParkingSpot, 0)
	for row, colVals := range parkingLot {
		for col, val := range colVals {
			if val == openSmallSpace || val == openRegularSpace || val == openMotorcycleSpace {
				esm[val] = append(esm[val], &ParkingSpot{column: col, row: row})
			}
		}
	}
	return esm
}
