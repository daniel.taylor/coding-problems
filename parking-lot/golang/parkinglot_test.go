package parking

import (
	"testing"
)

func TestFindEarliestSpotSimple(t *testing.T) {
	t.Run("Non-Existent Lot", nonExistentLotTest)
	t.Run("Full Single Space Lot", fullSingleSpaceSimpleLotTest)
	t.Run("Empty Single Space Lot", emptySingleSpaceSimpleLotTest)
	t.Run("Finding First Space", rectangularLotTest)
}

func TestFindEarliestSpotByStrictType(t *testing.T) {
	t.Run("Non-existent Lot", nonExistentStrictType)
	t.Run("Single Vehicle In Lot", largeLotStrictTypeOneVehicle)
	t.Run("Single Vehicle In Lot", largeLotStrictTypeOneOfEachVehicle)
	t.Run("Single Vehicle In Lot", largeLotStrictTypeTooManyOfAVehicle)
}

func nonExistentStrictType(t *testing.T) {
	parkingLot := make([][]rune, 0)
	vehicles := make([]rune, 0)
	spots := FindEarliestSpotByStrictType(parkingLot, vehicles)
	if len(spots) != 0 {
		t.Fatalf("There should have been no parking spots available.")
	}
}

func largeLotStrictTypeOneVehicle(t *testing.T) {
	parkingLot := createLargeLot()
	vehicles := []rune{'r'}
	spots := FindEarliestSpotByStrictType(parkingLot, vehicles)
	if len(spots) != 1 {
		t.Fatalf("Should have found one spot not %d.", len(spots))
	} else if spots[0].column != 0 || spots[0].row != 0 {
		t.Fatalf("Should have found a spot at (0,0) but was at (%d,%d).", spots[0].row, spots[0].column)
	}
}

func largeLotStrictTypeTooManyOfAVehicle(t *testing.T) {
	parkingLot := createLargeLot()
	vehicles := []rune{'m', 'm'}
	spots := FindEarliestSpotByStrictType(parkingLot, vehicles)
	if len(spots) != 2 {
		t.Fatalf("Should have found one spot not %d.", len(spots))
	} else if spots[0].column != 1 || spots[0].row != 2 {
		t.Fatalf("Should have found a spot at (2,1) but was at (%d,%d).", spots[0].row, spots[0].column)
	}
}

func largeLotStrictTypeOneOfEachVehicle(t *testing.T) {
	parkingLot := createLargeLot()
	vehicles := []rune{'r', 'm', 's'}
	spots := FindEarliestSpotByStrictType(parkingLot, vehicles)
	if len(spots) != 3 {
		t.Fatalf("Should have found one spot not %d.", len(spots))
	} else if spots[0].column != 0 || spots[0].row != 0 {
		t.Fatalf("Should have found a spot for regular vehicles at (0,0) but was at (%d,%d).", spots[0].row, spots[0].column)
	}
	if len(spots) != 3 {
		t.Fatalf("Should have found one spot for motorcycles not %d.", len(spots))
	} else if spots[1].column != 1 || spots[1].row != 2 {
		t.Fatalf("Should have found a spot for motorcycles at (2,1) but was at (%d,%d).", spots[1].row, spots[1].column)
	}
	if len(spots) != 3 {
		t.Fatalf("Should have found one spot not %d.", len(spots))
	} else if spots[2].column != 2 || spots[2].row != 0 {
		t.Fatalf("Should have found a spot for small vehicles at (0,2) but was at (%d,%d).", spots[2].row, spots[2].column)
	}
}

func createLargeLot() [][]rune {
	parkingLot := make([][]rune, 0)
	row := make([]rune, 0)
	row = append(row, 'r')
	row = append(row, 'R')
	row = append(row, 's')
	row = append(row, 'S')
	parkingLot = append(parkingLot, row)
	row = make([]rune, 0)
	row = append(row, 'M')
	row = append(row, 'M')
	row = append(row, 'R')
	row = append(row, 'S')
	parkingLot = append(parkingLot, row)
	row = make([]rune, 0)
	row = append(row, 'M')
	row = append(row, 'm')
	row = append(row, 'R')
	row = append(row, 'S')
	parkingLot = append(parkingLot, row)
	return parkingLot
}

func nonExistentLotTest(t *testing.T) {
	parkingLot := make([][]rune, 0, 0)
	spot, err := FindEarliestSpotSimple(parkingLot)
	if spot != nil || err.Error() != noSpot {
		t.Fatalf("There should have been no parking spots available.")
	}
}

func fullSingleSpaceSimpleLotTest(t *testing.T) {
	parkingLot := make([][]rune, 0)
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot[0] = append(parkingLot[0], '1')

	spot, err := FindEarliestSpotSimple(parkingLot)
	if spot != nil || err.Error() != noSpot {
		t.Fatalf("There should have been no parking spots available.")
	}
}

func emptySingleSpaceSimpleLotTest(t *testing.T) {
	parkingLot := make([][]rune, 0)
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot[0] = append(parkingLot[0], '0')

	spot, err := FindEarliestSpotSimple(parkingLot)
	if err != nil || (spot.column != 0 && spot.row != 0) {
		t.Fatalf("The spot should have been found at (0,0)")
	}
}

func rectangularLotTest(t *testing.T) {
	parkingLot := make([][]rune, 0)
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot = append(parkingLot, make([]rune, 0))
	parkingLot[0] = append(parkingLot[0], '1')
	parkingLot[0] = append(parkingLot[0], '1')
	parkingLot[0] = append(parkingLot[0], '1')
	parkingLot[0] = append(parkingLot[0], '1')
	parkingLot[0] = append(parkingLot[0], '1')
	parkingLot[1] = append(parkingLot[1], '1')
	parkingLot[1] = append(parkingLot[1], '1')
	parkingLot[1] = append(parkingLot[1], '0')
	parkingLot[1] = append(parkingLot[1], '1')
	parkingLot[1] = append(parkingLot[1], '1')
	parkingLot[2] = append(parkingLot[2], '1')
	parkingLot[2] = append(parkingLot[2], '1')
	parkingLot[2] = append(parkingLot[2], '1')
	parkingLot[2] = append(parkingLot[2], '1')
	parkingLot[2] = append(parkingLot[2], '1')
	parkingLot[3] = append(parkingLot[3], '1')
	parkingLot[3] = append(parkingLot[3], '1')
	parkingLot[3] = append(parkingLot[3], '1')
	parkingLot[3] = append(parkingLot[3], '1')
	parkingLot[3] = append(parkingLot[3], '0')
	parkingLot[4] = append(parkingLot[4], '0')
	parkingLot[4] = append(parkingLot[4], '0')
	parkingLot[4] = append(parkingLot[4], '0')
	parkingLot[4] = append(parkingLot[4], '0')
	parkingLot[4] = append(parkingLot[4], '0')

	spot, err := FindEarliestSpotSimple(parkingLot)
	if err != nil || (spot.column != 2 && spot.row != 1) {
		t.Fatalf("The spot should have been found at (1,2)")
	}
}
